<?php
while($fp = fgets(STDIN))
{
  $inputs[] = trim($fp);
}
array_shift($inputs);
$fp = fopen("php://stdout", "w+");
$number = 2;
$finish = 1000000;
$range = range($number,$finish);
$primes = array_combine($range,$range);
while($number*$number < $finish){
  for($i=$number; $i<=$finish; $i+=$number){
    if($i==$number){
      continue;
    }
    unset($primes[$i]);
  }
  $number = next($primes);
}

$primeFactors =  $primes;

foreach($inputs as $id => $num)
{
  foreach($primeFactors as $initialFactor)
  {
    $factor = $initialFactor;
    $moveOn = false;
    do{
    $a = (int) ($num/$factor);

    $b = $num - $a;

    $acheck = strpos($a,  "4");
    $bcheck = strpos($b,  "4");
    if(($acheck!==false) || ($bcheck!==false))
    {
      if($acheck!==false)
      {
        $increase = pow(10, strlen($a) - (strpos($a,  "4")+1));
      }
      elseif($bcheck!==false)
      {
        $increase = pow(10, strlen($b) - (strpos($b,  "4")+1));
      }
      $a = $a +$increase;
      $b = $b -$increase;
      $acheck = strpos($a,  "4");
      $bcheck = strpos($b,  "4");
    }

    $factor = $factor*2;

    if(strpos($b,  "4")===1 || strpos($a,  "4")===1 || strpos($b,  "4")===0 || strpos($a,  "4")===0)
    {
      $moveOn = true;
      break;
    }

    }while(($acheck!==false) || ($bcheck!==false));

    if($moveOn)
    {
      continue;
    }
    else
    {
      break;
    }
  }

  if($id==count($inputs)-1){
    fwrite($fp, "Case #".($id+1).": ".$a." ".$b);
  }
  else{
    fwrite($fp, "Case #".($id+1).": ".$a." ".$b.PHP_EOL);
  }
}
fclose($fp);



?>
