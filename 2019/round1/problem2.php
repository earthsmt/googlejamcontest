<?php

$itemCounter = 0;
while($fp = fgets(STDIN))
{
  $item[] =  trim($fp);
  if($itemCounter%2==0)
  {
      $inputs[] = $item;
      $item = array();
  }
  $itemCounter++;
}
array_shift($inputs);
$fp = fopen("php://stdout", "w+");

foreach($inputs as $id => $input)
{
  $n = $input[0];
  $moveSet = str_split($input[1]);

  $gridSet = array();
  $posX = 1;
  $posY = 1;

  foreach($moveSet as $move)
  {
    $orX = $posX;
    $orY = $posY;
    if($move=="S")
    {
      $posY +=1;
    }
    if($move=="E")
    {
      $posX +=1;
    }
    if(!isset($gridSet[$orX][$posX][$orY][$posY]))
    {
      $gridSet[$orX][$posX][$orY][$posY] = array();
    }
    $gridSet[$orX][$posX][$orY][$posY] = 1;
  }


  $myPosX = 1;
  $myPosY = 1;
  $pattern = "";
  $backTrackIndex = 0;
  do{
    $orX =$myPosX;
    $myPosX +=1;
    if(($myPosX <= $n) && !isset($gridSet[$orX][$myPosX][$myPosY][$myPosY]))
    {
      $pattern = $pattern."E";
    }
    else
    {
      $myPosX = $orX;
      $orY = $myPosY;
      $myPosY +=1;
      if(($myPosY <= $n) && !isset($gridSet[$orX][$orX][$orY][$myPosY]))
      {
        $pattern = $pattern."S";
      }
      else
      {
        if(!$backTrackIndex)
        {
          $myPosX -= 1;
          $sRemovals = strlen(substr($pattern, strrpos($pattern, "E")+1));
          $myPosY -=  $sRemovals;
          $pattern = substr($pattern,0, -($sRemovals+1));
          $pattern = $pattern."S";
        }
        else
        {
          $pattern = substr($pattern, 0, $backTrackIndex )."S";
          $myPosX = $backTrackX;
          $myPosY = $backTrackY +1;
        }
        $backTrackIndex = strlen($pattern);
        $backTrackX= $myPosX;
        $backTrackY = $myPosY;
      }
    }
  }while($myPosX !=$n  || $myPosY !=$n);

  if($id==count($inputs)-1){
    fwrite($fp, "Case #".($id+1).": ".$pattern);
  }
  else{
    fwrite($fp, "Case #".($id+1).": ".$pattern.PHP_EOL);
  }
}
fclose($fp);
?>
